/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.service;

public interface SpringTemplateService {
	
	public String getSpringTemplateSample();

}

