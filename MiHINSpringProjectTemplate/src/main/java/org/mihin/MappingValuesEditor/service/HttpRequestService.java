/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.service;

/**
 * @author Ahsan Raza
 *
 */
public interface HttpRequestService {

	public int sendGet(String base, String query, String authToken, StringBuffer response) throws Exception;

	public int sendPost(String base, String query, String data, String authToken, StringBuffer response) throws Exception;

	public int sendPut(String base, String query, String data, String authToken, StringBuffer response) throws Exception;

	public int sendDelete(String base, String query, String data, String authToken, StringBuffer response) throws Exception;

}
