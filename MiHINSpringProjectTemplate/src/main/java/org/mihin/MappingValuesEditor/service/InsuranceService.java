package org.mihin.MappingValuesEditor.service;

import java.util.List;

import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.Insurance34Value;


public interface InsuranceService {
	
	public boolean checkIfFieldsExist(int sendingFacilityId);
	
	public List<ConformanceMapping> getInsuranceValuesBySendingFacility(int sendingFacilityId);
	
}
