/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.service;

import java.util.List;

import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.SendingFacility;

public interface ConformanceMappingService {

	public List<String> getConformanceMappingFields(int sendingFacilityId);
	
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldName(int sendingFacilityId, String fieldName);
	
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldNameList(int sendingFacilityId, List<String> fieldNameList);

	public List<ConformanceMapping> getConformanceValuesBySendingFacility(int sendingFacilityId);
	public void deleteConformanceVals(int sendingFacilityId,String fieldName,List<String> fieldVals);

}

