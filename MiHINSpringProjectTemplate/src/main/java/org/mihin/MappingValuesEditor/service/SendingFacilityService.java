package org.mihin.MappingValuesEditor.service;

import java.util.List;

import org.mihin.MappingValuesEditor.model.SendingFacility;

public interface SendingFacilityService {

	public List<SendingFacility> findSendingFacilitiesByNameList(List<String> names);
	public List<SendingFacility> findAllSendingFacilities();
	public List<SendingFacility> sortSendingFacilities(List<SendingFacility> sendingFacilityList);
	public List<SendingFacility> findSendingFacilityByName(String sendingFacilityName);
}
