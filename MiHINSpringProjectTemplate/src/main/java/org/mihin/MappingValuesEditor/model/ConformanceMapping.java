package org.mihin.MappingValuesEditor.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the conformance_mapping database table.
 * 
 */
@Entity
@Table(name="conformance_mapping")
@NamedQuery(name="ConformanceMapping.findAll", query="SELECT c FROM ConformanceMapping c")
public class ConformanceMapping implements Serializable , Comparable<ConformanceMapping>{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="field_name")
	private String fieldName;

	@Column(name="field_value")
	private String fieldValue;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update")
	private Date lastUpdate;

	@Column(name="sending_facility_id")
	private int sendingFacilityId;

	public ConformanceMapping() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFieldName() {
		return this.fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getFieldValue() {
		return this.fieldValue;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public Date getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getSendingFacilityId() {
		return this.sendingFacilityId;
	}

	public void setSendingFacilityId(int sendingFacilityId) {
		this.sendingFacilityId = sendingFacilityId;
	}

	@Override
	public int compareTo(ConformanceMapping conformanceMapping) {
		return this.fieldName.compareTo(conformanceMapping.getFieldName());
	}

}