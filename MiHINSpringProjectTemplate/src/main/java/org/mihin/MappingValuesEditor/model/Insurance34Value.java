package org.mihin.MappingValuesEditor.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the insurance3_4_values database table.
 * 
 */
@Entity
@Table(name="insurance3_4_values")
@NamedQuery(name="Insurance34Value.findAll", query="SELECT i FROM Insurance34Value i")
public class Insurance34Value implements Serializable , Comparable<Insurance34Value>{
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="insurance3_1")
	private String insurance31;

	@Column(name="insurance4_1")
	private String insurance41;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="last_update")
	private Date lastUpdate;

	@Column(name="sending_facility_id")
	private int sendingFacilityId;

	public Insurance34Value() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getInsurance31() {
		return this.insurance31;
	}

	public void setInsurance31(String insurance31) {
		this.insurance31 = insurance31;
	}

	public String getInsurance41() {
		return this.insurance41;
	}

	public void setInsurance41(String insurance41) {
		this.insurance41 = insurance41;
	}

	public Date getLastUpdate() {
		return this.lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public int getSendingFacilityId() {
		return this.sendingFacilityId;
	}

	public void setSendingFacilityId(int sendingFacilityId) {
		this.sendingFacilityId = sendingFacilityId;
	}

	@Override
	public int compareTo(Insurance34Value ins34Val) {
		int compareVal = this.insurance31.compareTo(ins34Val.getInsurance31());
		if (compareVal==0) {
			return this.insurance41.compareTo(ins34Val.getInsurance41());
		}
		return compareVal;
	}



}