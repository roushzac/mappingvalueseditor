package org.mihin.MappingValuesEditor.model;

import java.io.Serializable;
import java.util.Comparator;

import javax.persistence.*;


/**
 * The persistent class for the sending_facility database table.
 * 
 */
@Entity
@Table(name="sending_facility")
@NamedQuery(name="SendingFacility.findAll", query="SELECT s FROM SendingFacility s")
public class SendingFacility implements Serializable,Comparable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="sending_facility_id")
	private int sendingFacilityId;

	@Column(name="sending_facility_name")
	private String sendingFacilityName;
	//public static Comparator<SendingFacility> byalpha = Comparator.comparing(s->sendingFacilityName);
	public SendingFacility() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSendingFacilityId() {
		return this.sendingFacilityId;
	}

	public void setSendingFacilityId(int sendingFacilityId) {
		this.sendingFacilityId = sendingFacilityId;
	}

	public String getSendingFacilityName() {
		return this.sendingFacilityName;
	}
	
	
	public void setSendingFacilityName(String sendingFacilityName) {
		this.sendingFacilityName = sendingFacilityName;
	}

	

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		if (getSendingFacilityName() == null || ((SendingFacility) o).getSendingFacilityName() == null) {
			return 0;
	    }
	    return getSendingFacilityName().compareTo(((SendingFacility) o).getSendingFacilityName());
		
	}

}