package org.mihin.MappingValuesEditor.daoImpl;

import java.math.BigInteger;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.mihin.MappingValuesEditor.dao.AbstractJpaDAO;
import org.mihin.MappingValuesEditor.dao.ConformanceMappingDAO;
import org.mihin.MappingValuesEditor.dao.InsuranceDAO;
import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.Insurance34Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public class InsuranceDAOImpl extends AbstractJpaDAO<Insurance34Value> implements InsuranceDAO {
	
	@Override
	public int checkInsuranceValueExists(int sendingFacilityId){
		String sql = "select if( exists( select * from insurance3_4_values where sending_facility_id=:sendingFacilityId), 1, 0)";
		Query q = entityManager.createNativeQuery(sql);
		q.setParameter("sendingFacilityId", sendingFacilityId);
		Integer result = ((BigInteger) q.getSingleResult()).intValue();
		return result;
	}

	
	@Override
	public List<Insurance34Value> getInsuranceValuesBySendingFacility(int sendingFacilityId){
		TypedQuery<Insurance34Value> q = entityManager.createQuery("FROM Insurance34Value p where p.sendingFacilityId=:sendingFacilityId", Insurance34Value.class);
		q.setParameter("sendingFacilityId", sendingFacilityId);
        List<Insurance34Value> insuranceList = q.getResultList();
        return insuranceList;
	}
	
}
