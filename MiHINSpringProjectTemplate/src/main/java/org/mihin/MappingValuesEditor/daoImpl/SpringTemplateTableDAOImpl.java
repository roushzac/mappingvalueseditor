/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.daoImpl;

import java.util.List;

import javax.persistence.Query;

import org.mihin.MappingValuesEditor.dao.AbstractJpaDAO;
import org.mihin.MappingValuesEditor.dao.SpringTemplateTableDAO;
import org.mihin.MappingValuesEditor.model.SpringTemplateTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
public class SpringTemplateTableDAOImpl extends AbstractJpaDAO<org.mihin.MappingValuesEditor.model.SpringTemplateTable> implements SpringTemplateTableDAO {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public SpringTemplateTableDAOImpl() {
		setClazz(org.mihin.MappingValuesEditor.model.SpringTemplateTable.class);
	}

	@Override
	@SuppressWarnings("unchecked")
	public SpringTemplateTable getSpringTemplate() {
		logger.debug("Get Spring Template Table DAO");
		
		String sql = "select * from spring_template_table where 1=1 AND id=:id ";
		Query q = entityManager.createNativeQuery(sql);
		q.setParameter("id", 1);
		List<Object[]> list = q.getResultList();
		for (Object[] obj : list) {
			SpringTemplateTable stt = new SpringTemplateTable();
			if (obj.length > 0) stt.setTemplateType(obj[1].toString());
			return stt;
		}
		return null;
	}

}
