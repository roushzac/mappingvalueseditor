package org.mihin.MappingValuesEditor.daoImpl;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.mihin.MappingValuesEditor.dao.AbstractJpaDAO;
import org.mihin.MappingValuesEditor.dao.ConformanceMappingDAO;
import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.SendingFacility;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Component
public class ConformanceMappingDAOImpl extends AbstractJpaDAO<ConformanceMapping> implements ConformanceMappingDAO{
	
	@Override
	public List<String> getConformanceFields(int sendingFacilityId){
		String sql = "select distinct field_name from conformance_mapping where sending_facility_id=:sendingFacilityId ";
		Query q = entityManager.createNativeQuery(sql);
		q.setParameter("sendingFacilityId", sendingFacilityId);
		List<String> fieldList = q.getResultList();
		//for (Object[] obj : list) {
		//	SpringTemplateTable stt = new SpringTemplateTable();
		//	if (obj.length > 0) stt.setTemplateType(obj[1].toString());
		//	return stt;
		//}
		return fieldList;
	}
	
	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldName(int sendingFacilityId, String fieldName){
		TypedQuery<ConformanceMapping> q = entityManager.createQuery("FROM ConformanceMapping p where p.sendingFacilityId=:sendingFacilityId and p.fieldName=:fieldName", ConformanceMapping.class);
		q.setParameter("sendingFacilityId", sendingFacilityId);
		q.setParameter("fieldName", fieldName);
        List<ConformanceMapping> conformanceMappingList = q.getResultList();
        return conformanceMappingList;
	}
	
	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldNameList(int sendingFacilityId, List<String> fieldNameList){
		TypedQuery<ConformanceMapping> q = entityManager.createQuery("FROM ConformanceMapping p where p.sendingFacilityId=:sendingFacilityId and p.fieldName IN :fieldNameList", ConformanceMapping.class);
		q.setParameter("sendingFacilityId", sendingFacilityId);
		q.setParameter("fieldNameList", fieldNameList);
        List<ConformanceMapping> conformanceMappingList = q.getResultList();
        return conformanceMappingList;
	}

	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacility(int sendingFacilityId){
		TypedQuery<ConformanceMapping> q = entityManager.createQuery("FROM ConformanceMapping p where p.sendingFacilityId=:sendingFacilityId", ConformanceMapping.class);
		q.setParameter("sendingFacilityId", sendingFacilityId);
		List<ConformanceMapping> conformanceMappingList = q.getResultList();

		return conformanceMappingList;
	}
	@Override
	@Transactional
	public void deleteVals (int sendingFacilityId,String fieldName,List<String> fieldVals){
		for (int i = 0; i < fieldVals.size(); i++) {
			String fieldval = fieldVals.get(i);
			String query = "DELETE FROM rhapsodyadtreporting.conformance_mapping  WHERE (sending_facility_id = "
					+sendingFacilityId + ") and (field_name= '"+ fieldName + "') and (field_value = '"+ fieldval+"')";
			entityManager.joinTransaction();
			Query q = entityManager.createNativeQuery(query);
			q.executeUpdate();
		}
	}
}
