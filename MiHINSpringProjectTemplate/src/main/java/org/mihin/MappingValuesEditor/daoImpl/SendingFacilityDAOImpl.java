/*package org.mihin.MappingValuesEditor.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import org.mihin.MappingValuesEditor.dao.AbstractJpaDAO;
import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.SendingFacility;
// dont know if component helps
@Component
@Repository
public class SendingFacilityDAOImpl  extends AbstractJpaDAO<SendingFacility> implements SendingFacilityDAO {
	
	public SendingFacilityDAOImpl()
	{
		setClazz(SendingFacility.class);
	}
	
	public SendingFacility FindFacilityByName(String name)
	{
		TypedQuery<SendingFacility> query = entityManager.createQuery(" from sending_facility p where p.name:=name", SendingFacility.class);
		query.setParameter("name", name);
		SendingFacility facility = query.getSingleResult();
		return facility;
	}
	
	public List<SendingFacility> FindAllFacilities()
	{
		System.out.println("You did it (DAO) !");
		return findAll();
	}

}*/
/*
 * This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
 */
package org.mihin.MappingValuesEditor.daoImpl;

import java.util.List;

import javax.persistence.TypedQuery;

import org.mihin.MappingValuesEditor.dao.AbstractJpaDAO;
import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.SendingFacility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;


@Repository
@Component
public class SendingFacilityDAOImpl extends AbstractJpaDAO<SendingFacility> implements SendingFacilityDAO {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	public SendingFacilityDAOImpl() {
		setClazz(SendingFacility.class);
	}

	@Override
	public List<SendingFacility> findAllSendingFacilities() {
		logger.debug("Retreiving Sending Facilities");
		List<SendingFacility> sendingFacilityList = findAll();
		return sendingFacilityList;
	}

	@Override
	public List<SendingFacility> findSendingFacilityByName(String name)
	{
		String queryString = "from SendingFacility p where p.sendingFacilityName = :name";
		TypedQuery<SendingFacility> query = entityManager.createQuery(queryString, SendingFacility.class);
		query.setParameter("name", name);
		return query.getResultList();
	}

	@Override
	public List<SendingFacility> findSendingFacilitiesByNameList(List<String> names)
	{
		TypedQuery<SendingFacility> query = entityManager.createQuery(" from SendingFacility p where p.sendingFacilityName IN :nameList", SendingFacility.class);
		query.setParameter("nameList", names);
		List<SendingFacility> sendingFacilityList = query.getResultList();
		return sendingFacilityList;
	}
//
//	private final Logger logger = LoggerFactory.getLogger(getClass());
//
//	public SpringTemplateTableDAOImpl() {
//		setClazz(org.mihin.springtemplate.model.SpringTemplateTable.class);
//	}
//
//	@Override
//	@SuppressWarnings("unchecked")
//	public SpringTemplateTable getSpringTemplate() {
//		logger.debug("Get Spring Template Table DAO");
//
//		String sql = "select * from spring_template_table where 1=1 AND id=:id ";
//		Query q = entityManager.createNativeQuery(sql);
//		q.setParameter("id", 1);
//		List<Object[]> list = q.getResultList();
//		for (Object[] obj : list) {
//			SpringTemplateTable stt = new SpringTemplateTable();
//			if (obj.length > 0) stt.setTemplateType(obj[1].toString());
//			return stt;
//		}
//		return null;
//	}

}
