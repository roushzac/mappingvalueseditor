package org.mihin.MappingValuesEditor.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.typesafe.config.Config;

@Component
public class SpringTemplateUtils {
	private final static Logger logger = LoggerFactory.getLogger(SpringTemplateUtils.class);
	private static Config properties;
	
	@Autowired
	public SpringTemplateUtils(Config config) {
		SpringTemplateUtils.properties = config;
	}
	
	public static String processSpringTemplate() {
		logger.debug("Processing Spring Template Utility");	
		return properties.getString("org.mihin.MappingValuesEditor.ENVIRONMENT");
	}
    

}
