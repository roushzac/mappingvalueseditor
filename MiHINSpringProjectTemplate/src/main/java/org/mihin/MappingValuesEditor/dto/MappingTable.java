package org.mihin.MappingValuesEditor.dto;

import org.mihin.MappingValuesEditor.model.ConformanceMapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MappingTable {

    HashMap<String, List<String>> tableMap = new HashMap<String, List<String>>();;
    List<String> msh4Vals;
    List<String> genderPID8Vals;
    List<String> racePID10Vals;
    List<String> ethPID22Vals;
    List<String> deathPID30Vals;
    List<String> diagDG16Vals;
    List<String> patientPV12Vals;
    List<String> AdmissionPV14Vals;
    List<String> AdmitPV114Vals;
    List<String> patientPV118Vals;
    List<String> HospServPV110Vals;
    List<String> DischargePV136;
    List<String> InsurIN117;
    List<String> InsurIN1;
    public HashMap<String, List<String>> getTableMap(){
        return this.tableMap;
    }
    List<String> get(String key)
    {
        List<String> result = new ArrayList<String>();
        if (tableMap.get(key) != null){
            result = tableMap.get(key);
        }

        return result;
    }
    List<String> getDG1_6(){
        String key = "DG1_6_DiagnosisType";
        return this.get(key);
    }
    List<String> getI_117(){
        String key = "IN1_17_1_InsuredRelationshipToPatient";
        return this.get(key);
    }
    List<String> getPID10Race(){
        String key = "PID10_Race";
        return this.get(key);
    }
    List<String> getPV14(){
        String key = "PV1_4_AdmissionType";
        return this.get(key);
    }
    List<String> getSendingFacilityOid(){
        String key = "SendingFacilityOid";
        return this.get(key);
    }
    List<String> getPV114Admit(){
        String key = "PV1_4_AdmissionSource";
        return this.get(key);
    }
    List<String> getPV114Patient(){
        String key = "PV1_4_PatientType";
        return this.get(key);
    }
    List<String> getDeathPID30Vals(){
        String key = "PID_30_PatientDeathIndicator";
        return this.get(key);
    }
    List<String> getGenderPID8Vals(){
        String key = "PID8_AdministrativeSex";
        return this.get(key);
    }
    List<String> getHospServPV110Vals(){
        String key = "PV1_10_HospitalService";
        return this.get(key);
    }
    List<String> getInpatient(){
        String key = "Inpatient";
        return this.get(key);
    }
    List<String> getEthPID22Vals(){
        String key = "PID_22_EthnicGroup";
        return this.get(key);
    }
    List<String> getPatientClass(){
        String key = "PV1_2_PatientClass";
        return this.get(key);
    }
    List<String> getEmergency(){
        String key = "Emergency";
        return this.get(key);
    }
    List<String> getOutPatient(){
        String key = "Outpatient";
        return this.get(key);
    }
    List<String> getDischargePV136(){
        String key = "PV1_36_DischargeDisposition";
        return this.get(key);
    }
    public MappingTable(List<ConformanceMapping> values){
        for(int i=0;i<values.size();i++){
            //System.out.println(values.get(i).getFieldValue());
            String keyField = values.get(i).getFieldName();
            String FieldValue = values.get(i).getFieldValue();
            // check if the field name is in the dictionary
            boolean containsKey = tableMap.containsKey(keyField);
            List<String> fieldVals = new ArrayList<String>();
            // if list does not exist create it
            if(containsKey==false){

                //fieldVals = new ArrayList<String>();
                fieldVals.add(FieldValue);
                // put it in the dictionary
                tableMap.put(keyField, fieldVals);
            }
            // otherwise just add to the list and overwrite the dictionary
            else {


                fieldVals = tableMap.get(keyField);
                fieldVals.add(FieldValue);
                tableMap.put(keyField, fieldVals);
            }
        }
    }

}
