/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.security;

import org.mihin.MappingValuesEditor.config.CustomAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.session.HttpSessionEventPublisher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
 
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
    }
    
    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
    
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
			.anyRequest().authenticated().and().formLogin()
			.loginPage("/login").permitAll()
			.loginProcessingUrl("/login")
			.defaultSuccessUrl("/home")
			.failureUrl("/login")
			.and().httpBasic();

		http.logout().permitAll()
			.logoutUrl("/logout")
			.logoutSuccessUrl("/login").invalidateHttpSession(true)
			.deleteCookies("JSESSIONID");

		http.csrf().ignoringAntMatchers("/login*");
		http.addFilterAfter(new CustomAuthenticationFilter(), BasicAuthenticationFilter.class);
		http.sessionManagement().maximumSessions(5);
		http.sessionManagement().invalidSessionUrl("/error-page");
		http.headers().frameOptions().disable();
	}
    
    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/");
        web.ignoring().antMatchers("/login");
        web.ignoring().antMatchers("/error-page");
        web.ignoring().antMatchers("/resources/static/**");
        web.ignoring().antMatchers("/home");
        web.ignoring().antMatchers("/submitSendingFacility");
        web.ignoring().antMatchers("/deleteMappingVals");
    }
    
    @Bean
    public HttpSessionEventPublisher httpSessionEventPublisher() {
        return new HttpSessionEventPublisher();
    }
}