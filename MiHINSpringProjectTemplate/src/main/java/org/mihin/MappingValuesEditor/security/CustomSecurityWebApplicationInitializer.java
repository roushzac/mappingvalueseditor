/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

public class CustomSecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
