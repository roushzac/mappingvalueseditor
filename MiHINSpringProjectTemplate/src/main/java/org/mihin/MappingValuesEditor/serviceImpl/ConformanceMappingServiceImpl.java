/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.mihin.MappingValuesEditor.dao.ConformanceMappingDAO;
import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.SendingFacility;
import org.mihin.MappingValuesEditor.service.ConformanceMappingService;
import org.mihin.MappingValuesEditor.service.HttpRequestService;
import org.mihin.MappingValuesEditor.service.SendingFacilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typesafe.config.Config;


@Service("conformanceMappingService")
public class ConformanceMappingServiceImpl implements ConformanceMappingService {

	@Autowired
	ConformanceMappingDAO conformanceMappingDAO;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Override
	public List<String> getConformanceMappingFields(int sendingFacilityId) {
		logger.debug("Retrieve Conformance Fields");
		ArrayList<String> fieldList = (ArrayList<String>) conformanceMappingDAO.getConformanceFields(sendingFacilityId);
		return fieldList;
	}
	
	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldName(int sendingFacilityId, String fieldName){
		return sortConformanceValues(conformanceMappingDAO.getConformanceValuesBySendingFacilityAndFieldName(sendingFacilityId, fieldName));
	}
	
	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldNameList(int sendingFacilityId,
			List<String> fieldNameList){
		return sortConformanceValues(conformanceMappingDAO.getConformanceValuesBySendingFacilityAndFieldNameList(sendingFacilityId, fieldNameList));
	}
	
	public List<ConformanceMapping> sortConformanceValues(List<ConformanceMapping> conformanceMappingList) {
		Collections.sort(conformanceMappingList);
		ArrayList<ConformanceMapping> conformanceMappingFilteredList = new ArrayList<ConformanceMapping>();
		for (ConformanceMapping conformanceMapping : conformanceMappingList) {
			String conformanceMappingValue = conformanceMapping.getFieldValue();
			if (conformanceMappingValue != null && !conformanceMappingValue.isEmpty() && !conformanceMappingValue.trim().isEmpty()) {
				conformanceMappingFilteredList.add(conformanceMapping);
			}
			//ConformanceMapping = (conformanceMappingList != null && !conformanceMappingList.isEmpty() && !conformanceMappingList.trim().isEmpty());
		}
		return conformanceMappingFilteredList;
	}

	@Override
	public List<ConformanceMapping> getConformanceValuesBySendingFacility(int sendingFacilityId) {
		return sortConformanceValues(conformanceMappingDAO.getConformanceValuesBySendingFacility(sendingFacilityId));
	}
	@Override
	public void deleteConformanceVals(int sendingFacilityId,String fieldName,List<String> fieldVals){
		conformanceMappingDAO.deleteVals(sendingFacilityId,fieldName,fieldVals);
	}

}
