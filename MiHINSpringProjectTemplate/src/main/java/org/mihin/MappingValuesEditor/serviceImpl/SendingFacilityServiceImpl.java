/*
package org.mihin.MappingValuesEditor.serviceImpl;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.SendingFacility;
import org.mihin.MappingValuesEditor.service.SendingFacilityService;

@Service("SendingFacilityService")
public class SendingFacilityServiceImpl implements SendingFacilityService {
	
	@Autowired
    SendingFacilityDAO FacilityDAO;
	
	@Override
	public SendingFacility findFacilityByName(String name) {
		SendingFacility Facility = FacilityDAO.FindFacilityByName(name);
		return Facility;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SendingFacility> FindAllFacilities() {
		List<SendingFacility> facilities = FacilityDAO.FindAllFacilities();
		//List<SendingFacility> sortedList = facilities.stream().sorted(Comparator.comparing(SendingFacility::getSendingFacilityName)).collect(Collectors.toList());
		//facilities.sort(SendingFacility.byalpha);
		Collections.sort(facilities);
		// take all the underscores out of the names
		// Hospital_1 becomes Hospital 1 
		// for every facility 
		for (int i =0; i < facilities.size(); i++)
		{
			// get the name 
			String name = facilities.get(i).getSendingFacilityName();
			// take underscores out of that name, replace it with space
			String Newname = name.replaceAll("_", " ");
			// set the new name 
			facilities.get(i).setSendingFacilityName(Newname);
			
		}
			
			
			
		return facilities;
	}

}
*/
package org.mihin.MappingValuesEditor.serviceImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.mihin.MappingValuesEditor.dao.SendingFacilityDAO;
import org.mihin.MappingValuesEditor.model.SendingFacility;
import org.mihin.MappingValuesEditor.service.HttpRequestService;
import org.mihin.MappingValuesEditor.service.SendingFacilityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typesafe.config.Config;


@Service("sendingFacilityService")
public class SendingFacilityServiceImpl implements SendingFacilityService {

	@Autowired
	SendingFacilityDAO sendingFacilityDAO;

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public List<SendingFacility> findSendingFacilityByName(String name) {
		List<SendingFacility> sendingFacilityList = sendingFacilityDAO.findSendingFacilityByName(name);
		return sortSendingFacilities(sendingFacilityList);
	}

	@Override
	public List<SendingFacility> findSendingFacilitiesByNameList(List<String> names) {
		List<SendingFacility> sendingFacilityList = sendingFacilityDAO.findSendingFacilitiesByNameList(names);
		return sortSendingFacilities(sendingFacilityList);
	}

	@Override
	public List<SendingFacility> findAllSendingFacilities() {
		//logger.debug("Getting Spring Template Sample");
		List<SendingFacility> sendingFacilityList = sendingFacilityDAO.findAllSendingFacilities();
		return sortSendingFacilities(sendingFacilityList);
	}

	public List<SendingFacility> sortSendingFacilities(List<SendingFacility> sendingFacilityList) {
		Collections.sort(sendingFacilityList);
		for (SendingFacility sendingFacility : sendingFacilityList) {
			String name = sendingFacility.getSendingFacilityName();
			String newName = name.replaceAll("_"," ");
			sendingFacility.setSendingFacilityName(newName);
		}
		return sendingFacilityList;
	}


}
