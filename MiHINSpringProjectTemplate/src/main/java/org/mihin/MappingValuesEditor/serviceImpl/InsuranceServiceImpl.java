package org.mihin.MappingValuesEditor.serviceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mihin.MappingValuesEditor.dao.InsuranceDAO;
import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.Insurance34Value;
import org.mihin.MappingValuesEditor.service.InsuranceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("insuranceService")
public class InsuranceServiceImpl implements InsuranceService {

	@Autowired
	InsuranceDAO insuranceDAO;
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	public boolean checkIfFieldsExist(int sendingFacilityId) {
		logger.debug("Retrieve Insurance Fields");
		if(insuranceDAO.checkInsuranceValueExists(sendingFacilityId) == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public List<ConformanceMapping> getInsuranceValuesBySendingFacility(int sendingFacilityId){
		return sortInsuranceValues(insuranceDAO.getInsuranceValuesBySendingFacility(sendingFacilityId));
	}
	
	public List<ConformanceMapping> sortInsuranceValues(List<Insurance34Value> insuranceList) {
		Collections.sort(insuranceList);
		ArrayList<ConformanceMapping> insuranceFilteredList = new ArrayList<ConformanceMapping>();
		for (Insurance34Value insurance : insuranceList) {
			String insurance3Value = insurance.getInsurance31();
			String insurance4Value = insurance.getInsurance41();
			if (insurance3Value != null && !insurance3Value.isEmpty() && !insurance3Value.trim().isEmpty()) {
				ConformanceMapping conformanceMapping = new ConformanceMapping();
				conformanceMapping.setFieldName("Insurance");
				conformanceMapping.setFieldValue(insurance3Value+" : "+insurance4Value);
				conformanceMapping.setLastUpdate(insurance.getLastUpdate());
				conformanceMapping.setSendingFacilityId(insurance.getSendingFacilityId());
				insuranceFilteredList.add(conformanceMapping);
			}
		}
		return insuranceFilteredList;
	}

}

