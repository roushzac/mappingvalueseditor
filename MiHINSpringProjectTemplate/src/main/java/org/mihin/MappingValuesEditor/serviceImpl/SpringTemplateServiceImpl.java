/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.serviceImpl;

import javax.servlet.http.HttpServletRequest;

import org.mihin.MappingValuesEditor.dao.SpringTemplateTableDAO;
import org.mihin.MappingValuesEditor.service.HttpRequestService;
import org.mihin.MappingValuesEditor.service.SpringTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.typesafe.config.Config;


@Service("springTemplateService")
public class SpringTemplateServiceImpl implements SpringTemplateService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	SpringTemplateTableDAO springTemplateTableDAO;
	@Autowired
	HttpRequestService httpRequestService;
	@Autowired
	HttpServletRequest request;
	@Autowired
	private Config properties;
	
	@Override
	public String getSpringTemplateSample() {
		
		logger.debug("Getting Spring Template Sample");
		return "Spring Template " + properties.getString("org.mihin.MappingValuesEditor.ENVIRONMENT");
	}
	
	
}
