/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.controller;

import javax.servlet.http.HttpServletRequest;

import org.mihin.MappingValuesEditor.dto.MappingTable;
import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.service.ConformanceMappingService;
import org.mihin.MappingValuesEditor.service.SendingFacilityService;
import org.mihin.MappingValuesEditor.service.SpringTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.mihin.MappingValuesEditor.model.SendingFacility;

import java.util.*;

import com.typesafe.config.Config;

@Controller
public class HomeController {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	SendingFacilityService sendingFacilityService;
	@Autowired
	SpringTemplateService springTemplateService;
	@Autowired
	ConformanceMappingService MappingSevice;
	//ADD sending facility service
	@Autowired
	HttpServletRequest request;
	@Autowired
	private Config properties;
    
/*    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
		ModelAndView model = new ModelAndView();
    	if (SecurityContextHolder.getContext().getAuthentication() != null && SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
    		logger.info("User Already Authenticated!"); 		
    		return home();
    	}
		model.setViewName("login");
		model.addObject("msg", request.getSession().getAttribute("loginErrorMessage")); request.getSession().removeAttribute("loginErrorMessage");
		
        return model;       
    }*/
/*	@RequestMapping(value = "/ok", method = RequestMethod.GET)
	public ModelAndView sayHello() {
		//System.out.println("You did it (Login Controller)!");
		ModelAndView mv = new ModelAndView("login");
		List<SendingFacility> sendingFacilities = FacilityService.FindAllFacilities();
		//mv.addObject("sendingFacilities", sendingFacilities);
		mv.addObject("form", new Form());

		return mv;
	}*/
    
/*    @RequestMapping(value = "/login-auth", method = RequestMethod.POST)
    public ModelAndView login1() {
    	ModelAndView model = new ModelAndView();
    	if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
    		return home();
    	}
    	else {
    		model.setViewName("login");
    	}
    	return model;  
    }*/



	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public ModelAndView home(@RequestParam(value="name", required=false) String sendingFacilityName, HttpServletRequest request) {
		List<SendingFacility> sendingFacilityList;

		if (sendingFacilityName==null) {
			sendingFacilityList = sendingFacilityService.findAllSendingFacilities();
		} else if (sendingFacilityName.contains(",")) {
			List<String> sendingFacilityNameList = Arrays.asList(sendingFacilityName.split("\\s*,\\s*"));
			sendingFacilityList = sendingFacilityService.findSendingFacilitiesByNameList(sendingFacilityNameList);
		} else {
			sendingFacilityList = sendingFacilityService.findSendingFacilityByName(sendingFacilityName);
		}
		ModelAndView mv;
		int size = sendingFacilityList.size();
		if (size == 0) {
			mv = new ModelAndView("redirect:/error-page?msg="+"User is not associated with a valid ADT Conformance Sending Facility.");
		} else {
			mv = new ModelAndView("home");
			mv.addObject("sendingFacilityList", sendingFacilityList);
		}
		return mv;
	}

    @RequestMapping(value = "/error-page", method = RequestMethod.GET)
    public ModelAndView errorpage(){
		ModelAndView model = new ModelAndView();
		model.addObject("msg", "Not Authenticated!");
		model.setViewName("error-page");
        return model;
    }
	@RequestMapping(value = "/zach", method = RequestMethod.GET)
	public ModelAndView zachpage(){
		ModelAndView model = new ModelAndView();
		model.addObject("msg", "zach page!");
		model.setViewName("error-page");
		return model;
	}

	@RequestMapping(value = "/submitSendingFacility", method = RequestMethod.POST)
	public @ResponseBody MappingTable submitSendingFacility(@RequestParam("sendingFacility") int sendingFacility)
	{
		System.out.println("Made it to Home Controller zach !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		// get hospital id from sending facility service
		//int hospitalID = sendingFacilityService.findSendingFacilityByName(sendingFacility).get(0).getId();
		List<ConformanceMapping> values = MappingSevice.getConformanceValuesBySendingFacility(sendingFacility);
		//Collections.sort(values);
		// for loop to get values

		MappingTable table = new MappingTable(values);
		HashMap<String, List<String>>  TableMap = table.getTableMap();
		List<String> keys = new ArrayList<String>(TableMap.keySet());
		return table;
	};
	@RequestMapping(value = "/deleteMappingVals", method = RequestMethod.POST)
	public @ResponseBody MappingTable deleteMappingVals(@RequestParam("sendingFacility") int sendingFacility,
														@RequestParam("fieldName") String FieldName,
														@RequestParam("fieldvals") String fieldvals)
	{
		List<String> FieldVals = new ArrayList<String>();
		FieldVals.add(fieldvals);
		MappingSevice.deleteConformanceVals(sendingFacility,FieldName,FieldVals);
		List<ConformanceMapping> values = MappingSevice.getConformanceValuesBySendingFacility(sendingFacility);

		System.out.println("You made it to the delete part");
		MappingTable table = new MappingTable(values);
		return table;
	};
    
}
