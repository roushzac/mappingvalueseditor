/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.config;

import java.io.File;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesConfigurer;
import org.springframework.web.servlet.view.tiles3.TilesViewResolver;

import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import com.typesafe.config.ConfigValueFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "org.mihin.MappingValuesEditor")
public class WebMvcConfig implements WebMvcConfigurer  {

    @Bean
    public TilesConfigurer tilesConfigurer() {
        TilesConfigurer tilesConfigurer = new TilesConfigurer();
        tilesConfigurer.setDefinitions(
          new String[] { "/WEB-INF/tiles-def.xml" });
        tilesConfigurer.setCheckRefresh(true);
         
        return tilesConfigurer;
    }
     
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        TilesViewResolver viewResolver = new TilesViewResolver();
        registry.viewResolver(viewResolver);
    }
     
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**")
          .addResourceLocations("/resources/");
    }
    
	@Bean
	public Config properties() throws Exception {
	Config conf = ConfigFactory
			.parseFile(new File("C:/Users/Zachary Roush.MIHIN-1720/.IntelliJIdea2018.3/system/tomcat" + System.getProperty("file.separator")
						+ "properties" + System.getProperty("file.separator") + "MappingValuesEditor.properties"));

	System.out.println(System.getProperty("catalina.base"));
	
		// Set Log Level
		Logger root = (Logger)LoggerFactory.getLogger("org.mihin.MappingValuesEditor");
		//root.setLevel(getLogLevel(conf.getString("org.mihin.MappingValuesEditor.LOG_LEVEL")));
		
		//AWSKMS kms = AWSKMSClientBuilder.defaultClient();
		//conf = addNewPropertyToConfig(conf,decrypt(kms, conf.getString("org.mihin.MappingValuesEditor.DATABASE_PASSWORD")), "org.mihin.MappingValuesEditor.DATABASE_PASSWORD");

		return conf;
	}

	private Config addNewPropertyToConfig(Config newConf, String decryptedString, String property) {
		ConfigValue cv = ConfigValueFactory.fromAnyRef(decryptedString);
		newConf = newConf.withValue(property, cv);
		return newConf;
	}
	
	private String decrypt(AWSKMS kms, String password) {
		String decodedString = "";
		ByteBuffer ciphertext = ByteBuffer.wrap(com.amazonaws.util.Base64.decode(password));
		DecryptRequest decreq = new DecryptRequest().withCiphertextBlob(ciphertext);
		ByteBuffer plaintext = kms.decrypt(decreq).getPlaintext();
		decodedString = Charset.forName("UTF-8").decode(plaintext).toString();
		return decodedString;
	}
	
	private Level getLogLevel(String level) {
		Level l = Level.INFO;
		if (level != null) {
			if ("debug".equalsIgnoreCase(level)) l = Level.DEBUG;
			else if ("info".equalsIgnoreCase(level)) l = Level.INFO;
			else if ("error".equalsIgnoreCase(level)) l = Level.ERROR;
			else if ("off".equalsIgnoreCase(level)) l = Level.OFF;
			else if ("all".equalsIgnoreCase(level)) l = Level.ALL;
		}
		return l;
	}

	
}
