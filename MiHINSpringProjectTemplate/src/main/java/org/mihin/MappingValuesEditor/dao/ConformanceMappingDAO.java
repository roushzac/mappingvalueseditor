package org.mihin.MappingValuesEditor.dao;

import java.util.List;

import org.mihin.MappingValuesEditor.model.ConformanceMapping;

public interface ConformanceMappingDAO {

	public List<String> getConformanceFields(int sendingFacilityId);
	
	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldName(int sendingFacilityId, String fieldName);

	public List<ConformanceMapping> getConformanceValuesBySendingFacilityAndFieldNameList(int sendingFacilityId, List<String> fieldNameList);

	public List<ConformanceMapping> getConformanceValuesBySendingFacility(int sendingFacilityId);
	public void deleteVals (int sendingFacilityId,String fieldName,List<String> fieldVals);

}
