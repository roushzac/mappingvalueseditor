/*package org.mihin.MappingValuesEditor.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import org.mihin.MappingValuesEditor.model.SendingFacility;

public interface SendingFacilityDAO {
	
	
	
	public SendingFacility FindFacilityByName(String name);
	
	
	public List<SendingFacility> FindAllFacilities();
	
}*/
package org.mihin.MappingValuesEditor.dao;

import java.util.List;

import org.mihin.MappingValuesEditor.model.SendingFacility;

public interface SendingFacilityDAO {

	public List<SendingFacility> findAllSendingFacilities();

	public List<SendingFacility> findSendingFacilitiesByNameList(List<String> names);

	public List<SendingFacility> findSendingFacilityByName(String name);

}

