/*
* This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
*/
package org.mihin.MappingValuesEditor.dao;

import org.mihin.MappingValuesEditor.model.SpringTemplateTable;
import org.springframework.transaction.annotation.Transactional;

public interface SpringTemplateTableDAO {
	
	@Transactional
	public SpringTemplateTable getSpringTemplate();

}
