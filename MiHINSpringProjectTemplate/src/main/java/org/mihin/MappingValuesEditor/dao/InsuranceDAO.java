package org.mihin.MappingValuesEditor.dao;

import java.util.List;

import org.mihin.MappingValuesEditor.model.ConformanceMapping;
import org.mihin.MappingValuesEditor.model.Insurance34Value;

public interface InsuranceDAO {
	
	public int checkInsuranceValueExists(int sendingFacilityId);

	public List<Insurance34Value> getInsuranceValuesBySendingFacility(int sendingFacilityId);

}