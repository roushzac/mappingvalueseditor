/**
	This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
**/
$(document).ready(function() {

	var token = $("meta[name='_csrf']").attr("content");
	var header = $("meta[name='_csrf_header']").attr("content");
	$(document).ajaxSend(function(e, xhr, options) {
		if (token && header)
			xhr.setRequestHeader(header, token);
	});
	$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	
	$('form').submit(function(event) {
		$('form').append('<input type="hidden"  name="_csrf"   value="'+$("meta[name='_csrf']").attr("content")+'"/>');
	});
	
	/**
	$('#loginbtn').click(function() {
		$.ajax({
			url : 'SpringTemplate/login-auth', //$('#base_url').val()+
			data : {
				'username' : $('#username').val(),
				'password' : $('#password').val(),
			},
			dataType : 'json',
			type: 'POST',
			success : function(data) {
				console.log(data);
				window.location.href = 'SpringTemplate/home';  // $('#base_url').val()+
			},
			error : function(error) {
				console.log(error);
				window.location.href = 'SpringTemplate/home'; // $('#base_url').val()+
			}
		});
	});
	**/
});