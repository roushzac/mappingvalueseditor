<!--
	This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
-->

<link rel="stylesheet" href="resources/static/css/springtemplate.css">
<link rel="stylesheet" href="resources/static/css/tether.min.css">
<link rel="stylesheet" href="resources/static/css/jquery-ui.min.css">
<link rel="stylesheet" href="resources/static/css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="resources/static/css/datatables.min.css">


<script src="resources/static/js/jquery-3.3.1.min.js"></script>
<script src="resources/static/js/jquery-ui.min.js"></script>
<script src="resources/static/js/tether.min.js"></script>
<script src="resources/static/js/bootstrap.min.js"></script>
<script src="resources/static/js/jquery.blockUI.js"></script>
<script src="resources/static/js/springtemplate.js"></script>
<script src="resources/static/js/datatables.min.js"></script>

