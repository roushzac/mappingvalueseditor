<!--
	This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
-->
<%@ page import="java.text.SimpleDateFormat" %>    
 <%
 	SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
 	java.util.Date date = new java.util.Date();
 %>
 <div id="footer">
  	<div class="container-fluid">
	    <p class="float-right"><a href="#">Back to top</a></p>
	   	<p class="text-muted credit copyright">Copyright <%=sdf.format(date)%> &#169; Michigan Health Information Network Shared Services
	   	<br /><a href="https://mihin.org" target="_blank">mihin.org</a></p>
  	</div>
</div>