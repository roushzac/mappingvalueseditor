<!--
	This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
-->
<%@ include file="/WEB-INF/jsp/common/include.jsp"%>
<nav class="navbar navbar-toggleable-md navbar-light" style="background-color: #e8e8e8;">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#">Spring Template</a>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     <!--<li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li> -->
      <!--<li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>  -->
      <!--<li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>  -->
    </ul>
    <sec:authorize access="hasAnyRole('ROLE_USER', 'ROLE_ADMIN')">
   		<c:url var="logout" value="/logout" />
		<form action="${logout}" method="post"><input type="submit" class="btn btn-default btn-sm" value="Log out" /></form>
	</sec:authorize>
  </div>
</nav>