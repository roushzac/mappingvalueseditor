<!--
	This code is confidential proprietary trade secret of MiHIN. Copyright MiHIN 2018.
-->
<script src="resources/static/scripts/home.js" type=text/javascript></script>

<%@ include file="/WEB-INF/jsp/common/include.jsp"%>
<style>
	#values {
		font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
	}

	#values td, #values th {
		border: 1px solid #ddd;
		padding: 8px;
	}

	#values tr:nth-child(even){background-color: #f2f2f2;}

	#values tr:hover {background-color: #ddd;}

	#values th {
		padding-top: 12px;
		padding-bottom: 12px;
		text-align: left;
		background-color: #4CAF50;
		color: white;
	}
</style>

<div id="body">
	<div class="container-fluid">
	
		<hr />
		<div id="messages">
			${msg}
			<br />
				${env}
		</div>

	</div>

	<br />

	<p>This is a paragraph.</p>
	<form:form action="login" method="GET" modelAttribute="form">
	<label for="hospital">Hospital </label>

		<select path="hospital" id="hospital">
			<c:forEach var="sendingFacility" items="${sendingFacilityList}">
				<option value="${sendingFacility.sendingFacilityId}">${sendingFacility.sendingFacilityName}</option>
			</c:forEach>
		</select>

	</form:form>
	<input type="submit" value="Submit" class="myButton" onclick="submitSendingFacility()"/>
	<input type="submit" value="Submit" class="myButton2" onclick="deleteVal()"/>
	<table id ="values" style="display:none" >


	</table>
</div>